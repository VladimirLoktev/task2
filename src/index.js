const { initDB } = require("./database/config");
const Mark = require("./database/models/mark.model");
const Student = require("./database/models/student.model");
const fs = require("fs");
const Subject = require("./database/models/subject.model");

async function bootstrap() {
  await initDB();
  const subjectList = await Subject.findAll({
    attributes: {
      include: [
        [Sequelize.fn("COUNT", Sequelize.col("students.id")), "studentCount"],
      ],
    },
    include: [
      {
        model: Mark,
        required: false,
        attributes: [],
        include: [
          {
            model: Student,
            required: false,
            attributes: [],
          },
        ],
      },
    ],
    group: ["Subject.id"],
  });

  fs.writeFileSync(
    "./output.json",
    JSON.stringify({
      subjectList,
    })
  );
}

bootstrap();
