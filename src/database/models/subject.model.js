const { sequelize } = require("..");
const { Sequelize } = require("sequelize");
const Student = require("./student.model");
const Mark = require("./mark.model");

class Subject extends Sequelize.Model {}

Subject.init(
  {
    id: {
      type: Sequelize.DataTypes.UUID,
      defaultValue: Sequelize.DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
  },
  { sequelize: sequelize, modelName: "subject" }
);

Student.hasMany(Mark);

Mark.belongsTo(Student, {
  foreignKey: "studentId",
});

Subject.hasMany(Mark);

Mark.belongsTo(Subject, {
  foreignKey: "subjectId",
});

module.exports = Subject;
